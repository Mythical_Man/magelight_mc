package mc.adamwilliamson.magelight;


import net.minecraft.block.Block;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLPaths;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import mc.adamwilliamson.magelight.blocks.FirstBlock;
import mc.adamwilliamson.magelight.blocks.IMagelightBlock;
import mc.adamwilliamson.magelight.blocks.Infusor;
import mc.adamwilliamson.magelight.blocks.Infusor_Container;
import mc.adamwilliamson.magelight.blocks.Infusor_Tile;
import mc.adamwilliamson.magelight.blocks.ModBlocks;
import mc.adamwilliamson.magelight.blocks.Mystic_Fungus_Dissolve;
import mc.adamwilliamson.magelight.blocks.Mystic_Fungus_Force;
import mc.adamwilliamson.magelight.blocks.Mystic_Fungus_Repress;
import mc.adamwilliamson.magelight.blocks.Mystic_Generator;
import mc.adamwilliamson.magelight.blocks.Mystic_Generator_Container;
import mc.adamwilliamson.magelight.blocks.Mystic_Generator_Tile;
import mc.adamwilliamson.magelight.blocks.Mystic_Ore;
import mc.adamwilliamson.magelight.blocks.Mystic_Fungus_Solid;
import mc.adamwilliamson.magelight.blocks.Socotra_Log;
import mc.adamwilliamson.magelight.blocks.Socotra_Planks;
import mc.adamwilliamson.magelight.entities.WeirdMobEntity;
import mc.adamwilliamson.magelight.items.Mystic_Gem;
import mc.adamwilliamson.magelight.items.Socotra_Pickaxe;
import mc.adamwilliamson.magelight.items.Socotra_Stick;
import mc.adamwilliamson.magelight.items.WeirdMobEggItem;
import mc.adamwilliamson.magelight.setup.ClientProxy;
import mc.adamwilliamson.magelight.setup.IProxy;
import mc.adamwilliamson.magelight.setup.ServerProxy;

// The value here should match an entry in the META-INF/mods.toml file

@Mod(Magelight.MODID)

public class Magelight
{
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger(Magelight.MODID);
    public static final String MODID = "magelight";
   
    //DistExecutor can run code depending on which side your on (Client vs Server)
    // double lambda delays pulling in code until execution/
    public static IProxy proxy = DistExecutor.runForDist(() -> () -> new ClientProxy(), () -> () -> new ServerProxy());

    public static ModSetup setup = new ModSetup();
    
    public Magelight() {
    	ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, Config.CLIENT_CONFIG);
    	ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, Config.COMMON_CONFIG);
    	
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        // Register the enqueueIMC method for modloading
        //FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);  // Intr mod communication
        // Register the processIMC method for modloading
        //FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        // Register the doClientStuff method for modloading
        //FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);  // Doing another way.

       // Register ourselves for server and other game events we are interested in
        //MinecraftForge.EVENT_BUS.register(this);  // Not needed atm because we don't register other game events.
       
        Config.loadConfig(Config.CLIENT_CONFIG,  FMLPaths.CONFIGDIR.get().resolve("magelight-client.toml"));
        Config.loadConfig(Config.COMMON_CONFIG,  FMLPaths.CONFIGDIR.get().resolve("magelight-common.toml"));
        
    }

   private void setup(final FMLCommonSetupEvent event)
    {
        setup.init();
        proxy.init();
    }
    
    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
    	// Blocks in the world.
        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> event) {
            // register a new block here
            LOGGER.info("HELLO from Register Block");
            event.getRegistry().register(new FirstBlock());
            event.getRegistry().register(new Socotra_Log());
            event.getRegistry().register(new Socotra_Planks());
            event.getRegistry().register(new Infusor());
            event.getRegistry().register(new Mystic_Ore());
            event.getRegistry().register(new Mystic_Generator());
            event.getRegistry().register(new Mystic_Fungus_Solid());
            event.getRegistry().register(new Mystic_Fungus_Repress());
            event.getRegistry().register(new Mystic_Fungus_Dissolve());
            event.getRegistry().register(new Mystic_Fungus_Force());
        }
        
        // Block Items are for inventory.
        @SubscribeEvent
        public static void onItemsRegistry(final RegistryEvent.Register<Item> event) {
        	try {
	        	RegisterBlockAsItem(event, ModBlocks.FIRSTBLOCK);
	        	RegisterBlockAsItem(event, ModBlocks.SOCOTRA_LOG);

        	RegisterBlockAsItem(event, ModBlocks.SOCOTRA_PLANKS);
	        	RegisterBlockAsItem(event, ModBlocks.INFUSOR);
	        	RegisterBlockAsItem(event, ModBlocks.MYSTIC_ORE);
	        	RegisterBlockAsItem(event, ModBlocks.MYSTIC_GENERATOR);
	        	RegisterBlockAsItem(event, ModBlocks.MYSTIC_FUNGUS_SOLID);
	        	RegisterBlockAsItem(event, ModBlocks.MYSTIC_FUNGUS_REPRESS);
	        	RegisterBlockAsItem(event, ModBlocks.MYSTIC_FUNGUS_DISSOLVE);
	        	RegisterBlockAsItem(event, ModBlocks.MYSTIC_FUNGUS_FORCE);
	        	
        	} 
        	catch (Exception e) {
        		LOGGER.error(e.getMessage());
        	}
        	
//        	Item.Properties properties = new Item.Properties()
//        			.group(setup.itemGroup);
//        	event.getRegistry().register(new BlockItem(ModBlocks.FIRSTBLOCK, properties).setRegistryName("firstblock"));
//        	event.getRegistry().register(new BlockItem(ModBlocks.SOCOTRA_LOG, new Item.Properties()).setRegistryName("socotra_log"));
//        	event.getRegistry().register(new BlockItem(ModBlocks.SOCOTRA_PLANKS, new Item.Properties()).setRegistryName("socotra_planks"));
//        	event.getRegistry().register(new BlockItem(ModBlocks.INFUSOR, new Item.Properties()).setRegistryName("infusor"));
//        	event.getRegistry().register(new BlockItem(ModBlocks.MYSTIC_ORE, new Item.Properties()).setRegistryName("mystic_ore"));
        	event.getRegistry().register(new Mystic_Gem());
        	event.getRegistry().register(new Socotra_Stick());
        	event.getRegistry().register(new Socotra_Pickaxe());
        	event.getRegistry().register(new WeirdMobEggItem());
        }
        
        @SubscribeEvent
        public static void onTileEntityRegistry(final RegistryEvent.Register<TileEntityType<?>> event) {
        	event.getRegistry().register(TileEntityType.Builder.create(Mystic_Generator_Tile::new, ModBlocks.MYSTIC_GENERATOR).build(null).setRegistryName("mystic_generator"));
        	event.getRegistry().register(TileEntityType.Builder.create(Infusor_Tile::new, ModBlocks.INFUSOR).build(null).setRegistryName("infusor"));
        }
        
        @SubscribeEvent
        public static void onContainerRegistry(final RegistryEvent.Register<ContainerType<?>> event)
        {        	
        	event.getRegistry().register(IForgeContainerType.create((windowId, inv, data) -> {
        		BlockPos pos = data.readBlockPos();
        		return new Mystic_Generator_Container(Magelight.proxy.getClientWorld(), pos, inv, proxy.getPlayer());
        	}).setRegistryName("mystic_generator"));
        	
        	event.getRegistry().register(IForgeContainerType.create((windowId, inv, data) -> {
				BlockPos pos = data.readBlockPos();
	    		return new Infusor_Container(Magelight.proxy.getClientWorld(), pos, inv, proxy.getPlayer());	
			}).setRegistryName("infusor"));
        }
        
        @SubscribeEvent
        public static void onEntityRegistry(final RegistryEvent.Register<EntityType<?>> event) {
        	event.getRegistry().register(EntityType.Builder.create(WeirdMobEntity::new, EntityClassification.CREATURE).size(1,1)
        			.setShouldReceiveVelocityUpdates(false)
        			.build("weirdmob")
        			.setRegistryName(Magelight.MODID, "weirdmob"));
        }
        
        private static void RegisterBlockAsItem(final RegistryEvent.Register<Item> event, Block block) throws Exception {

       	IMagelightBlock mb = (block instanceof IMagelightBlock) ? (IMagelightBlock) block : null;
        	if (mb == null) throw new Exception("Cannot register this block using this function");
        	
        	Item.Properties properties = new Item.Properties()
        			.group(setup.itemGroup);
        	event.getRegistry().register(new BlockItem(block, properties).setRegistryName(mb.getName()));
        }
        
    }

//    private void doClientStuff(final FMLClientSetupEvent event) {
//        // do something that can only be done on the client
//        LOGGER.info("Got game settings {}", event.getMinecraftSupplier().get().gameSettings);
//    }
//
//    private void enqueueIMC(final InterModEnqueueEvent event)
//    {
//        // some example code to dispatch IMC to another mod
//        InterModComms.sendTo("examplemod", "helloworld", () -> { LOGGER.info("Hello world from the MDK"); return "Hello world";});
//    }
//
//    private void processIMC(final InterModProcessEvent event)
//    {
//        // some example code to receive and process InterModComms from other mods
//        LOGGER.info("Got IMC {}", event.getIMCStream().
//                map(m->m.getMessageSupplier().get()).
//                collect(Collectors.toList()));
//    }
//    // You can use SubscribeEvent and let the Event Bus discover methods to call
//    @SubscribeEvent
//    public void onServerStarting(FMLServerStartingEvent event) {
//        // do something when the server starts
//        LOGGER.info("HELLO from server starting");
//    }

}
