package mc.adamwilliamson.magelight.blocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import mc.adamwilliamson.magelight.Config;
import mc.adamwilliamson.magelight.Constants;
import mc.adamwilliamson.magelight.crafting.BaseCraftingTile;
import mc.adamwilliamson.magelight.crafting.infusor.InfusorCraftingManager;
import mc.adamwilliamson.magelight.tools.CapabilityHeat;
import mc.adamwilliamson.magelight.tools.CustomEnergyStorage;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class Infusor_Tile extends BaseCraftingTile implements ITickableTileEntity 
{
	private LazyOptional<IEnergyStorage> energy = LazyOptional.of(this::createEnergy);
	private LazyOptional<IEnergyStorage> heat = LazyOptional.of(this::createHeat);
	
	private final int HEATING_COUNTER = 0;
	private final int XP_COUNTER = 1;
	private final int RESULT_COUNTER = 2;
	private List<Integer> counters = new ArrayList<Integer>(Collections.nCopies(RESULT_COUNTER + 1, 0));
	
	//private boolean hasHeat = false;
	//private boolean hasXP = false;
	
	//private final int RESULT_TIMELENGTH = 30;
	
	public Infusor_Tile() 
	{
		super(ModBlocks.INFUSOR_TILE, InfusorCraftingManager.getInstance());
	}
	
	protected int getSlotCount() { return Infusor.SLOTCOUNT; }
	
	protected int getResultSlot() { return Infusor.RESULT_SLOT; }
	
	protected List<Integer> getIngredientSlots(){ return Arrays.asList(Infusor.ITEM1_SLOT, Infusor.ITEM2_SLOT); }
	
	@Override
	public void tick() 
	{
		if (world.isRemote)
		{
			return;
		}
		BlockState blockState = world.getBlockState(pos);

		int energyCount = energy.orElse(null).getEnergyStored();
//		energy.ifPresent(h -> {
//			energyCount = h.getEnergyStored();
//		});
		
		
		int heatCount = heat.orElse(null).getEnergyStored();
//		heat.ifPresent(h -> {
//			heatCount = h.getEnergyStored();
//		});
		
		inventoryHandler.ifPresent(h -> {
			if (h.getSlots() < Infusor.RESULT_SLOT)
			{
				return;
			}
			
			//  If There is fuel to heat, add heat, if heat is full, stop buring fuel.
			// If there is XP to drain, drain xp, until it is full, then stop buring XP;
			//  if Heat and XP is high enough, craft.
			
			tickXP_Slot(h,blockState);
			
			tickHeat_Slot(h, blockState);
			
			tickCraft(h, blockState);
			
			markDirty();
		});
		
		
		if (heatCount > 0 && energyCount > 0)
		{
			world.setBlockState(pos, blockState.with(BlockStateProperties.POWERED, true), 3);
		}
		else
		{
			world.setBlockState(pos, blockState.with(BlockStateProperties.POWERED, false), 3);
		}
	}
	
	private boolean tickCraft(IItemHandler h, BlockState blockState)
	{
		//ItemStack ingredient1 = h.getStackInSlot(Infusor.ITEM1_SLOT);
		//ItemStack ingredient2 = h.getStackInSlot(Infusor.ITEM2_SLOT);
		
		//TODO: Replace with (Does this have a recipe)
		//this.craft();
		//BaseRecipe recipe = findMatchingRecipe();
		if (canCraft())
		{
			craft();
			return true;
			
//			if (hasHeat && hasXP) {
//				if (counters.get(RESULT_COUNTER) > 0) counters.set(RESULT_COUNTER, counters.get(RESULT_COUNTER) - 1);
//				
//			} else {
//				counters.set(RESULT_COUNTER, RESULT_TIMELENGTH); 
//			}
		}
		else
		{
			//counters.set(RESULT_COUNTER, RESULT_TIMELENGTH);
		}
		
		return false;
	}
	
	private void tickXP_Slot(IItemHandler h, BlockState blockState)
	{
		if (counters.get(XP_COUNTER) > 0)
		{
			counters.set(XP_COUNTER, counters.get(XP_COUNTER) - 1);
		} 
		else if (counters.get(XP_COUNTER) <= 0)
		{
			ItemStack stack = h.getStackInSlot(Infusor.XP_SLOT);
			if (Constants.XP_ITEMS.contains(stack.getItem()))
			{
				h.extractItem(Infusor.XP_SLOT, 1, false);
				counters.set(XP_COUNTER, Config.WEAK_TICKS_PER_XPBOTTLE.get());
				energy.ifPresent(e -> ((CustomEnergyStorage)e).addEnergy(Config.WEAK_POWER_GENERATE_XPBOTTLE.get()));
			}
		}
	}
	
	private void tickHeat_Slot(IItemHandler h, BlockState blockState)
	{
		if (counters.get(HEATING_COUNTER) > 0)
		{
			counters.set(HEATING_COUNTER, counters.get(HEATING_COUNTER) - 1);
		} 
		else if (counters.get(HEATING_COUNTER) <= 0)
		{
			ItemStack stack = h.getStackInSlot(Infusor.HEATING_SLOT);
			if (Constants.FUELS.contains(stack.getItem()))
			{
				h.extractItem(Infusor.HEATING_SLOT, 1, false);
				counters.set(HEATING_COUNTER, Config.WEAK_TICKS_PER_COAL.get());
				energy.ifPresent(e -> ((CustomEnergyStorage)e).addEnergy(Config.WEAK_HEAT_GENERATE_COAL.get()));
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private INBTSerializable<CompoundNBT> toSerializableCompound(IEnergyStorage h)
	{
		return (INBTSerializable<CompoundNBT>)h;
	}
	
	@Override
	public void read(CompoundNBT tag)
	{
		CompoundNBT energyTag = tag.getCompound(Constants.Energy);
		CompoundNBT heatTag = tag.getCompound(Constants.Heat);
		energy.ifPresent(h -> toSerializableCompound(h).deserializeNBT(energyTag ));
		heat.ifPresent(h -> toSerializableCompound(h).deserializeNBT(heatTag));
		super.read(tag);
	}
	
	@Override
	public CompoundNBT write(CompoundNBT tag)
	{	
		energy.ifPresent(h -> {
			CompoundNBT compound = toSerializableCompound(h).serializeNBT();
			tag.put(Constants.Energy, compound);
		});
	
		heat.ifPresent(h -> {
			CompoundNBT compound = toSerializableCompound(h).serializeNBT();
			tag.put(Constants.Heat, compound);
		});
		
		return super.write(tag);
	}
	
	@Override
	protected IItemHandler createStackHadler()
	{
		return new InfusorStackHandler() {
			@Override
			protected void onContentsChanged(int slot) 
			{
				markDirty();
			}
		};
	}
	
	private IEnergyStorage createEnergy()
	{
		return new CustomEnergyStorage(Config.WEAK_POWER_MAXPOWER.get(), 0);
	}
	
	private IEnergyStorage createHeat()
	{
		return new CustomEnergyStorage(Config.WEAK_HEAT_MAX.get(), 0);
	}
	
	@Nonnull
	@Override
	public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side)
	{
		if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.orEmpty(cap, inventoryHandler);
		}
		if (cap == CapabilityEnergy.ENERGY) {
			return energy.cast();
		}
		if (cap == CapabilityHeat.HEAT) {
			return heat.cast();
		}
		return super.getCapability(cap, side);
	}

	@Override
	public Container createMenu(int i, PlayerInventory inventory, PlayerEntity entity) 
	{
		return new Infusor_Container(world, pos, inventory, entity);
	}

	@Override
	public ITextComponent getDisplayName()
	{
		return new TranslationTextComponent(getType().getRegistryName().toString());
	}
}
