package mc.adamwilliamson.magelight.blocks;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import mc.adamwilliamson.magelight.Config;
import mc.adamwilliamson.magelight.tools.CustomEnergyStorage;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class Mystic_Generator_Tile extends TileEntity implements ITickableTileEntity, INamedContainerProvider {

	private LazyOptional<IItemHandler> handler = LazyOptional.of(this::createHandler);
	private LazyOptional<IEnergyStorage> energy = LazyOptional.of(this::createEnergy);
	private int counter;
	
	public Mystic_Generator_Tile() {
		super(ModBlocks.MYSTIC_GENERATOR_TILE);
	}

	@Override
	public void tick() {
		if (world.isRemote) {
			return;
		}
		
		BlockState blockState = world.getBlockState(pos);
		if (counter > 0) {
			counter--;
			if (counter <= 0) {
				energy.ifPresent(e -> ((CustomEnergyStorage)e).addEnergy(Config.MYSTIC_GENERATOR_POWER_GENERATE.get()));
			}
			markDirty();
		} else {
			handler.ifPresent(h -> {
				ItemStack stack = h.getStackInSlot(0);
				if (stack.getItem() == Items.EXPERIENCE_BOTTLE) {
					h.extractItem(0, 1, false);
					counter = Config.MYSTIC_GENERATOR_TICKS_PER_XPBOTTLE.get();
					world.setBlockState(pos, blockState.with(BlockStateProperties.POWERED, true), 3);
					markDirty();  // Unnecessary but good learning.  extractItem does it.
				} else {
					world.setBlockState(pos, blockState.with(BlockStateProperties.POWERED, false), 3);
				}
			});
		}
		
//		
//		if (blockState.get(BlockStateProperties.POWERED) != counter > 0) {
//			world.setBlockState(pos, blockState.with(BlockStateProperties.POWERED,  stack.getItem() ), 3);
//		}
		
		sendOutPower();
	}
	
	
	private void sendOutPower() {
		energy.ifPresent(energy -> {
			AtomicInteger capacity = new AtomicInteger(energy.getEnergyStored());
			if (capacity.get() > 0) {
				for (Direction direction : Direction.values()) {
					TileEntity te = world.getTileEntity(pos.offset(direction));
					if (te != null) {
						boolean doContinue = te.getCapability(CapabilityEnergy.ENERGY, direction).map(h -> {
							if (h.canReceive()) {
								int recieved = h.receiveEnergy(Math.min(capacity.get(),  Config.MYSTIC_GENERATOR_POWER_SEND.get()), false);
								capacity.addAndGet(-recieved);
								((CustomEnergyStorage)h).consumeEnergy(recieved);
								markDirty();
								return capacity.get() > 0;
							}
							
							return true;
						}).orElse(true);
						
						if (!doContinue) {
							return;
						}
					}
				}
			}
		});
		
	}
	
	@Override
	public void read(CompoundNBT tag) {
		CompoundNBT invTag = tag.getCompound("inv");
		CompoundNBT energyTag = tag.getCompound("energy");
		handler.ifPresent(h -> ((INBTSerializable<CompoundNBT>)h).deserializeNBT(invTag));
		energy.ifPresent(h -> ((INBTSerializable<CompoundNBT>)h).deserializeNBT(energyTag ));
		//this.handler.deserializeNBT(invTag);
		super.read(tag);
	}
	
	public CompoundNBT write(CompoundNBT tag) {
		handler.ifPresent(h -> {
			CompoundNBT compound =  ((INBTSerializable<CompoundNBT>)h).serializeNBT();
			tag.put("inv", compound);
		});
		
		energy.ifPresent(h -> {
			CompoundNBT compound = ((INBTSerializable<CompoundNBT>)h).serializeNBT();
			tag.put("energy", compound);
		});
	
		return super.write(tag);
	}
	
	private IItemHandler createHandler() {
		Logger.getGlobal().info("MysticGeneratorTile- CreateHandler");
		return new ItemStackHandler(1) {
			
			@Override
			protected void onContentsChanged(int slot) {
				markDirty();
				//super.onContentsChanged(slot);
			}
			
			@Override
			public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
				return stack.getItem() == Items.EXPERIENCE_BOTTLE;
			}
			
			@Override
			public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
				if (stack.getItem() != Items.EXPERIENCE_BOTTLE) {
					return stack;
				}
				return super.insertItem(slot, stack, simulate);
			}
		};
	}
	
	private IEnergyStorage createEnergy() {
		return new CustomEnergyStorage(Config.MYSTIC_GENERATOR_POWER_MAXPOWER.get(), 0);
	}
	
	@Nonnull
	@Override
	public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side){
		if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return handler.cast();
		}
		if (cap == CapabilityEnergy.ENERGY) {
			return energy.cast();
		}
		
		return super.getCapability(cap, side);
	}

	@Override
	public Container createMenu(int i, PlayerInventory inventory, PlayerEntity entity) {
		// TODO Auto-generated method stub
		return new Mystic_Generator_Container(world, pos, inventory, entity);
	}

	@Override
	public ITextComponent getDisplayName() {
		// TODO Auto-generated method stub
		TranslationTextComponent comp = new TranslationTextComponent(getType().getRegistryName().toString());
		return comp;//new StringTextComponent(()).getFormattedText());
	}
}
