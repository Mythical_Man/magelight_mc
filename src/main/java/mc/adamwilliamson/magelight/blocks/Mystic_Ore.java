package mc.adamwilliamson.magelight.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class Mystic_Ore extends Block implements IMagelightBlock  {
	public String getName() { return "mystic_ore"; }
	public Mystic_Ore() {
		super(
			Properties
				.create(Material.PACKED_ICE)
				.sound(SoundType.CORAL)
				.hardnessAndResistance(50.0f)
				.lightValue(15)
		);
		setRegistryName(getName());
	}
}
