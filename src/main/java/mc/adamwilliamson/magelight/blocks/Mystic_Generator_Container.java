package mc.adamwilliamson.magelight.blocks;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.IntReferenceHolder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;
import mc.adamwilliamson.magelight.tools.CustomEnergyStorage;

public class Mystic_Generator_Container extends Container 
{
	final int InvStartX = 8;
	final int InvStartY = 84;
	//final int SlotWidth = 65;
	//final int SlotHeight = 65;
	//final int SlotDistance = 6;
	
	final int InputSlotX = 79;
	final int InputSlotY = 53;
	
	private TileEntity tileEntity;
	private IItemHandler playerInventory; 
	
	public Mystic_Generator_Container( World world, BlockPos pos, PlayerInventory inventory, PlayerEntity player)
	{
		super(ModBlocks.MYSTIC_GENERATOR_CONTAINER, 1);
		tileEntity = world.getTileEntity(pos);
		playerInventory = new InvWrapper(inventory);

		tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {
			addSlot(new SlotItemHandler(h, 0, InputSlotX, InputSlotY));
		});
		
		layoutPlayerInventorySlots(InvStartX, InvStartY);
		
		trackInt(new IntReferenceHolder()
		{
			@Override
			public int get()
			{
				return getEnergy();
			}
			
			@Override
			public void set(int value)
			{
				tileEntity.getCapability(CapabilityEnergy.ENERGY).ifPresent(h -> ((CustomEnergyStorage)h).setEnergy(value));
			}
		});
	}
	
	public int getEnergy()
	{
		return tileEntity.getCapability(CapabilityEnergy.ENERGY).map(IEnergyStorage::getEnergyStored).orElse(0);
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) 
	{
		return isWithinUsableDistance(IWorldPosCallable.of(tileEntity.getWorld(), tileEntity.getPos()), playerIn, ModBlocks.MYSTIC_GENERATOR);
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index)
	{
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);
		if (slot != null)
		{
			ItemStack stack = slot.getStack();
			itemstack = stack.copy();
			if (index == 0)
			{
				if (!this.mergeItemStack(stack, 1, 37, true))
				{
					return ItemStack.EMPTY;
				}
				slot.onSlotChange(stack,  itemstack);;
			}
			else 
			{
				if (stack.getItem() == Items.EXPERIENCE_BOTTLE)
				{
					if (!this.mergeItemStack(stack, 0, 1, false))
					{
						return ItemStack.EMPTY;
					} 
					else if (index < 28)
					{
						if (!this.mergeItemStack(stack,  28,  37,  false))
						{
							return ItemStack.EMPTY;
						}
					}
					else if (index < 37 && !this.mergeItemStack(stack, 1, 28, false))
					{
						return ItemStack.EMPTY;
					}
				}
			}
			
			if (stack.isEmpty())
			{
				slot.putStack(ItemStack.EMPTY);
			}
			else
			{
				slot.onSlotChanged();
			}
			
			if (stack.getCount() == itemstack.getCount())
			{
				return ItemStack.EMPTY;
			}
			
			slot.onTake(playerIn,  stack);
		}
		
		return itemstack;
	}
	
	private int addSlotRange(IItemHandler handler, int index, int x, int y, int amount, int dx)
	{
		for(int i=0; i < amount; i++)
		{
			addSlot(new SlotItemHandler(handler, index, x, y));
			x += dx;
			index++;
		}
		
		return index;
	}
	
	private int addSlotBox(IItemHandler handler, int index, int x, int y, int horAmount, int dx, int verAmount, int dy)
	{
		for (int j=0; j < verAmount; j++)
		{
			index = addSlotRange(handler, index, x, y, horAmount, dx);
			y += dy;
		}
		return index;
	}
	
	private void layoutPlayerInventorySlots(int leftCol, int topRow)
	{
		addSlotBox(playerInventory, 9, leftCol, topRow, 9, 18, 3, 18);
		
		topRow +=58;
		addSlotRange(playerInventory, 0, leftCol, topRow, 9, 18);
		
	}
}
