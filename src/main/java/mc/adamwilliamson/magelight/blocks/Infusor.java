package mc.adamwilliamson.magelight.blocks;

import javax.annotation.Nullable;

import net.minecraft.block.Block;

import net.minecraft.block.BlockState;

import net.minecraft.block.SoundType;

import net.minecraft.block.material.Material;

import net.minecraft.entity.player.PlayerEntity;

import net.minecraft.entity.player.ServerPlayerEntity;

import net.minecraft.inventory.container.INamedContainerProvider;

import net.minecraft.state.StateContainer;

import net.minecraft.state.properties.BlockStateProperties;

import net.minecraft.tileentity.TileEntity;

import net.minecraft.util.Hand;

import net.minecraft.util.math.BlockPos;

import net.minecraft.util.math.BlockRayTraceResult;

import net.minecraft.world.IBlockReader;

import net.minecraft.world.World;

import net.minecraftforge.fml.network.NetworkHooks;

public class Infusor extends Block implements IMagelightBlock  
{
	public static final int HEATING_SLOT = 0;
	public static final int XP_SLOT = 1;
	public static final int ITEM1_SLOT = 2;
	public static final int ITEM2_SLOT = 3;
	public static final int RESULT_SLOT = 4;
	public static final int SLOTCOUNT = 5;
	
	public String getName() { return "infusor"; }
	
	public Infusor()
	{
		super(
			Properties
				.create(Material.IRON)
				.sound(SoundType.METAL)
				.hardnessAndResistance(2.0f)
				.lightValue(5)
		);
		setRegistryName(getName());
	}
	
	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public int getLightValue(BlockState state)
	{
		return state.get(BlockStateProperties.POWERED)? super.getLightValue(state) : 0;
	}

	@Nullable
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new Infusor_Tile();
	}
	
	@Override
	public void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
	{
		super.fillStateContainer(builder);
		builder.add(BlockStateProperties.POWERED);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult result) 
	{
		if (!world.isRemote) 
		{
			TileEntity tileEntity = world.getTileEntity(pos);
			//if (tileEntity instanceof INamedContainerProvider) 
			{
				NetworkHooks.openGui((ServerPlayerEntity)player,  (INamedContainerProvider) tileEntity, tileEntity.getPos());
			}
		}
		return super.onBlockActivated(state, world, pos, player, hand, result);
	}
}
