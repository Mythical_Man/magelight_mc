package mc.adamwilliamson.magelight.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CocoaBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.Block.Properties;
import net.minecraft.block.material.Material;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IWorldReader;

public class Mystic_Fungus_Dissolve extends Base_Mystic_Fungus {
	@Override
	public String getName() { return "mystic_fungus_dissolve"; }
	
	public Mystic_Fungus_Dissolve() {
		super();
	}
}
