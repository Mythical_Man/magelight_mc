package mc.adamwilliamson.magelight.blocks;

import net.minecraft.inventory.container.ContainerType;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.registries.ObjectHolder;

public class ModBlocks 
{
	@ObjectHolder("magelight:firstblock")
	public static FirstBlock FIRSTBLOCK;
	
	@ObjectHolder("magelight:socotra_log")
	public static Socotra_Log SOCOTRA_LOG;
	
	@ObjectHolder("magelight:socotra_planks")
	public static Socotra_Planks SOCOTRA_PLANKS;
	
	@ObjectHolder("magelight:infusor")
	public static Infusor INFUSOR;
	@ObjectHolder("magelight:infusor")
	public static TileEntityType<Infusor_Tile> INFUSOR_TILE;
	@ObjectHolder("magelight:infusor")
	public static ContainerType<Infusor_Container> INFUSOR_CONTAINER;
	
	@ObjectHolder("magelight:mystic_ore")
	public static Mystic_Ore MYSTIC_ORE;
	
	@ObjectHolder("magelight:mystic_generator")
	public static Mystic_Generator MYSTIC_GENERATOR;
	@ObjectHolder("magelight:mystic_generator")
	public static TileEntityType<Mystic_Generator_Tile> MYSTIC_GENERATOR_TILE;
	@ObjectHolder("magelight:mystic_generator")
	public static ContainerType<Mystic_Generator_Container> MYSTIC_GENERATOR_CONTAINER;
	
	@ObjectHolder("magelight:mystic_fungus_solid")
	public static Mystic_Fungus_Solid MYSTIC_FUNGUS_SOLID;
	@ObjectHolder("magelight:mystic_fungus_repress")
	public static Mystic_Fungus_Repress MYSTIC_FUNGUS_REPRESS;
	@ObjectHolder("magelight:mystic_fungus_dissolve")
	public static Mystic_Fungus_Dissolve MYSTIC_FUNGUS_DISSOLVE;
	@ObjectHolder("magelight:mystic_fungus_force")
	public static Mystic_Fungus_Force MYSTIC_FUNGUS_FORCE;
}
