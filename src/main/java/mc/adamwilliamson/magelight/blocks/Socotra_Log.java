package mc.adamwilliamson.magelight.blocks;

import net.minecraft.block.RotatedPillarBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class Socotra_Log extends RotatedPillarBlock  implements IMagelightBlock  {
	public String getName() { return "socotra_log"; }
	
	public Socotra_Log() {
		super(
			Properties
				.create(Material.CLAY)
				.sound(SoundType.WOOD)
				.hardnessAndResistance(2.0f)
				.lightValue(5)
		);
		setRegistryName(getName());
	}
}
