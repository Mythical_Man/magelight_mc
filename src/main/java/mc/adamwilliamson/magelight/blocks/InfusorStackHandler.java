package mc.adamwilliamson.magelight.blocks;

import javax.annotation.Nonnull;

import mc.adamwilliamson.magelight.Constants;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;

public class InfusorStackHandler extends ItemStackHandler 
{
	public InfusorStackHandler()
	{
		super(Infusor.SLOTCOUNT);
	}
	
	@Override
	public boolean isItemValid(int slot, @Nonnull ItemStack stack) 
	{
		if (slot == Infusor.RESULT_SLOT) 
		{
			return false;
		}
		else if (slot == Infusor.HEATING_SLOT) 
		{
			return Constants.FUELS.contains(stack.getItem());
		} 
		else if (slot == Infusor.XP_SLOT) 
		{
			return Constants.XP_ITEMS.contains(stack.getItem());	
		}
		
		return true;
	}
	
	@Override
	public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate)
	{
		if (slot == Infusor.XP_SLOT)
		{
			if (Constants.XP_ITEMS.stream().anyMatch(o -> o.getRegistryName() == stack.getItem().getRegistryName()))
			{
				return super.insertItem(Infusor.XP_SLOT, stack, simulate);	
			}
		} 
		else if (slot == Infusor.HEATING_SLOT) 
		{
			if ( Constants.FUELS.stream().anyMatch(o -> o.getRegistryName() == stack.getItem().getRegistryName()))
			{
				return super.insertItem(Infusor.HEATING_SLOT, stack, simulate);	
			}
		}
		else if (slot != Infusor.RESULT_SLOT)
		{
			return super.insertItem(slot, stack, simulate);
		}
		
		return stack;
	}
	
    @Override
    @Nonnull
    public ItemStack getStackInSlot(int slot)
    {
        validateSlotIndex(slot);
        return this.stacks.get(slot);
    }
}
