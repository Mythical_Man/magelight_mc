package mc.adamwilliamson.magelight.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class Socotra_Planks extends Block implements IMagelightBlock  {
	public String getName() { return "socotra_planks"; }
	
	public Socotra_Planks() {
		super(
			Properties
				.create(Material.CLAY)
				.sound(SoundType.WOOD)
				.hardnessAndResistance(2.0f)
				.lightValue(14)
		);
		setRegistryName(getName());
	}
}
