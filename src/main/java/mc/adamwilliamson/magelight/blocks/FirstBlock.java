package mc.adamwilliamson.magelight.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class FirstBlock extends Block implements IMagelightBlock {
	public String getName() { return "firstblock"; }
	
	public FirstBlock() {
		super(
			Properties
				.create(Material.IRON)
				.sound(SoundType.METAL)
				.hardnessAndResistance(2.0f)
				.lightValue(14)
		);
		setRegistryName(getName());
	}
}
