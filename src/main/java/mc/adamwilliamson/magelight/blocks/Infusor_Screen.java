package mc.adamwilliamson.magelight.blocks;

import com.mojang.blaze3d.platform.GlStateManager;

import mc.adamwilliamson.magelight.Magelight;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class Infusor_Screen extends ContainerScreen<Infusor_Container> {

	private ResourceLocation GUI = new ResourceLocation(Magelight.MODID, "textures/gui/infusor.png");
	
	public Infusor_Screen(
			Infusor_Container screenContainer, 
			PlayerInventory inv,
			ITextComponent titleIn) 
	{
		super(screenContainer, inv, titleIn);
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks) 
	{
		this.renderBackground();
		super.render(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}
	
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) 
	{
		this.font.drawString(this.title.getFormattedText(), 8.0f, 6.0f, 4210752);
		this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 120.0f, (float)(this.ySize - 96 + 2), 4210752);
		
		this.font.drawString(container.getEnergy() + "MP", 130, 10, 4210752);
		this.font.drawString(container.getHeat() + " Heat", 130, 20, 4210752);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) 
	{
		GlStateManager.color4f(1.0f,  1.0f,  1.0f,  1.0f);
		this.minecraft.getTextureManager().bindTexture(GUI);
		int RelX = (this.width - this.xSize)/ 2;
		int RelY= (this.height- this.ySize)/ 2;
		this.blit(RelX, RelY, 0, 0, this.xSize, this.ySize);
	}
}
