package mc.adamwilliamson.magelight.blocks;

import net.minecraft.block.Block;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.IntReferenceHolder;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import mc.adamwilliamson.magelight.container.BaseContainer;
import mc.adamwilliamson.magelight.tools.CapabilityHeat;
import mc.adamwilliamson.magelight.tools.CustomEnergyStorage;

public class Infusor_Container extends BaseContainer 
{
	private final int inputHeatSlotX = 36;
	private final int inputHeatSlotY = 36;
	
	private final int inputXPSlotX = 36;
	private final int inputXPSlotY = 60;
	
	private final int inputItem1SlotX = 88;
	private final int inputItem1SlotY = 22;
	
	private final int inputItem2SlotX = 88;
	private final int inputItem2SlotY = 50;
	
	private final int outputSlotX = 140;
	private final int outputSlotY = 36;
	
	private SlotItemHandler resultSlot;
	protected List<SlotItemHandler> ingredientSlots;
	
	public Infusor_Container( World world, BlockPos pos, PlayerInventory inventory, PlayerEntity player) 
	{
		super(
			ModBlocks.INFUSOR_CONTAINER, 
			2, 
			world, 
			pos, 
			inventory, 
			player, 
			Infusor.SLOTCOUNT);
		
		trackInt(new IntReferenceHolder() 
		{
			@Override
			public int get()
			{
				return getEnergy();
			}
			
			@Override
			public void set(int value)
			{
				tileEntity.getCapability(CapabilityEnergy.ENERGY).ifPresent(h -> ((CustomEnergyStorage)h).setEnergy(value));
			}
		});
		
		trackInt(new IntReferenceHolder()
		{
			@Override
			public int get()
			{
				return getHeat();
			}
			
			@Override
			public void set(int value)
			{
				tileEntity.getCapability(CapabilityHeat.HEAT).ifPresent(h -> ((CustomEnergyStorage)h).setEnergy(value));
			}
		});
	}
	
	@Override
	protected Map<Integer, Item> slotAllowances(){
		return new HashMap<Integer, Item>()
		{
			private static final long serialVersionUID = 5788959522447761150L;
			{
				put(0, Items.EXPERIENCE_BOTTLE);
				put(1, Items.COAL);
			}
		};
	}
	
	@Override
	protected void InitSlots(IItemHandler h) 
	{
		ingredientSlots = Lists.<SlotItemHandler>newArrayList();
		addSlot(new SlotItemHandler(h, Infusor.HEATING_SLOT, inputHeatSlotX, inputHeatSlotY));
		addSlot(new SlotItemHandler(h, Infusor.XP_SLOT, inputXPSlotX, inputXPSlotY));
		SlotItemHandler item1 = new SlotItemHandler(h, Infusor.ITEM1_SLOT, inputItem1SlotX, inputItem1SlotY); 
		addSlot(item1);
		SlotItemHandler item2 =new SlotItemHandler(h, Infusor.ITEM2_SLOT, inputItem2SlotX, inputItem2SlotY); 
		addSlot(item2);
		ingredientSlots.add(item1);
		ingredientSlots.add(item2);
		resultSlot = new SlotItemHandler(h, Infusor.RESULT_SLOT, outputSlotX, outputSlotY);
		addSlot(resultSlot);
	}

	@Override
	protected void contentsChanged(Container container, NonNullList<ItemStack> items) {}
	@Override
	protected void slotChanged(Container container,  int slot, ItemStack stack) {}
	@Override
	protected void windowPropertyChanged(Container containerIn, int varToUpdate, int newValue) {}
	
	@Override
	protected Block getBlock() {
		return ModBlocks.INFUSOR;
	}
	
	public int getEnergy() 
	{
		return tileEntity.getCapability(CapabilityEnergy.ENERGY).map(IEnergyStorage::getEnergyStored).orElse(0);
	}
	
	public int getHeat() 
	{
		return tileEntity.getCapability(CapabilityHeat.HEAT).map(IEnergyStorage::getEnergyStored).orElse(0);
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) 
	{
		return isWithinUsableDistance(IWorldPosCallable.of(tileEntity.getWorld(), tileEntity.getPos()), playerIn, ModBlocks.INFUSOR);
	}
}
