package mc.adamwilliamson.magelight.container;

import net.minecraft.block.Block;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.ClickType;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.IContainerListener;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

import java.util.Map;
import java.util.logging.Logger;

import mc.adamwilliamson.magelight.blocks.Infusor;
import mc.adamwilliamson.magelight.crafting.BaseCraftingTile;


public abstract class BaseContainer extends Container implements IContainerListener, IInventory
{
	private final int InvStartX = 8;
	private final int InvStartY = 84;
	
	protected final int slotCount;
	private final int inventorySlotMax;
	private final int quickSlotMax;
	
	protected IItemHandler playerInventory;
	protected TileEntity tileEntity;
	protected IItemHandler itemHandler;
	protected World world;
	
	public BaseContainer( 
		ContainerType<?> type, 
		int id,
		World worldIn,
		BlockPos pos,
		PlayerInventory inventory,
		PlayerEntity player,
		int slotCountIn) 
	{
		super(type, id);
		
		slotCount = slotCountIn;
		inventorySlotMax = slotCount + 9 * 3;
		quickSlotMax = inventorySlotMax + 9;
		world = worldIn;
		playerInventory = new InvWrapper(inventory);
		tileEntity = worldIn.getTileEntity(pos);
		
		if (tileEntity instanceof BaseCraftingTile)
		{
			((BaseCraftingTile)tileEntity).setInventory(this);
		}
		
		tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
			.ifPresent(h -> {
				itemHandler = h;
				InitSlots(h);
			});
		
		layoutPlayerInventorySlots();
		super.addListener(this);
	}
	
	public void sendAllContents(Container container, NonNullList<ItemStack> items) 
	{
		this.contentsChanged(container, items);
	}
	public void sendSlotContents(Container container, int slot, ItemStack stack)
	{
		this.slotChanged(container, slot, stack);
	}
	public void sendWindowProperty(Container containerIn, int varToUpdate, int newValue)
	{
		this.windowPropertyChanged(containerIn, varToUpdate, newValue);
	}
	
	protected abstract void contentsChanged(Container container, NonNullList<ItemStack> items);
	protected abstract void slotChanged(Container container,  int slot, ItemStack stack);
	protected void windowPropertyChanged(Container containerIn, int varToUpdate, int newValue) {}
	
	protected abstract void InitSlots(IItemHandler h);
	protected abstract Block getBlock();
	protected abstract Map<Integer, Item> slotAllowances();
	
	@Override
	public boolean canInteractWith(PlayerEntity playerIn) 
	{
		return isWithinUsableDistance(IWorldPosCallable.of(tileEntity.getWorld(), tileEntity.getPos()), playerIn, getBlock());
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index)
	{
		Logger.getGlobal().info("Transfering Stack in Slot");
		return super.transferStackInSlot(playerIn, index);
	}
	
	@Override
	public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, PlayerEntity player)
	{
		Logger.getGlobal().info("Slot Click");
		return super.slotClick(slotId,  dragType,  clickTypeIn,  player);
	}
	
	private int addSlotRange(IItemHandler handler, int index, int startXPos, int startYPos, int slotCount, int slotXSpacing)
	{
		for(int i = 0; i < slotCount; i++)
		{
			addSlot(new SlotItemHandler(handler, index, startXPos, startYPos));
			startXPos += slotXSpacing;
			index++;
		}
		
		return index;
	}
	
	private int addSlotBox(IItemHandler handler, int index, int startXPos, int startYPos, int horSlotCount, int slotXSpacing, int verSlotCount, int slotYSpacing)
	{
		for (int j=0; j < verSlotCount; j++)
		{
			index = addSlotRange(handler, index, startXPos, startYPos, horSlotCount, slotXSpacing);
			startYPos += slotYSpacing;
		}
		return index;
	}
	
	private void layoutPlayerInventorySlots()
	{
		addSlotBox(playerInventory, 9, InvStartX, InvStartY, 9, 18, 3, 18);
		int topRow = InvStartY+58;
		addSlotRange(playerInventory, 0, InvStartX, topRow, 9, 18);
	}
	
	@Override
	public ItemStack getStackInSlot(int index) 
	{
		return index >= this.getSizeInventory() ? ItemStack.EMPTY : getSlot(index).getStack();//.stackList.get(index);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) 
	{
		ItemStack items = super.getSlot(index).getStack();
		super.putStackInSlot(index, ItemStack.EMPTY);
		return items;
		//return ItemStackHelper.getAndRemove(this.getInventory(), index);//.stackList
	}

	@Override
	public ItemStack decrStackSize(int index, int count) 
	{
		ItemStack itemstack = ItemStackHelper.getAndSplit(this.getInventory(), index, count);
		if (!itemstack.isEmpty()) 
		{
			this.onCraftMatrixChanged(this);
		}

		return itemstack;
	}
   
	@Override
	public int getSizeInventory() 
	{
		return super.inventorySlots.size();
	}

	@Override
	public boolean isEmpty() 
	{
		for(Slot itemstack : super.inventorySlots)
		{
			if (itemstack.getStack() != null && !itemstack.getStack().isEmpty()) 
		   	{
		   		return false;
         	}
		}

		return true;
	}
	
	@Override
   	public void setInventorySlotContents(int index, ItemStack stack) 
	{
		super.putStackInSlot(index, stack);
	      //this.stackList.set(index, stack);
		//super.inventorySlots.get(index).putStack(stack);
		//this.onCraftMatrixChanged(this);
	}

	@Override
	public void markDirty() {	
	}

	@Override
	public boolean isUsableByPlayer(PlayerEntity player) 
	{
		return true;
	}

	@Override
	public void clear() 
	{
		for (int i = 0; i < Infusor.SLOTCOUNT; i++)
		{
			super.putStackInSlot(i, ItemStack.EMPTY);
		}
	}
}
