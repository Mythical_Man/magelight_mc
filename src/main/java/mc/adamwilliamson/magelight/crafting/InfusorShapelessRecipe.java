package mc.adamwilliamson.magelight.crafting;

import java.util.List;

import com.google.common.collect.Lists;

import mc.adamwilliamson.magelight.blocks.Infusor;
import net.minecraft.item.crafting.IRecipeType;

public class InfusorShapelessRecipe extends BaseShapelessRecipe// IRecipe<InfusorCraftingInventory>
{
    public InfusorShapelessRecipe(RecipeBuilder.ShapelessRecipeBase base)
    {
    	super(base);
    }

	@Override
	public IRecipeType<IInfusorRecipe> getType() {
		// TODO Auto-generated method stub
		return InfusorRecipeType.INFUSOR;
	}

	@Override
	protected List<Integer> getCraftingSlots(){
		return Lists.<Integer>newArrayList(Infusor.ITEM1_SLOT, Infusor.ITEM2_SLOT);
		
	}
	
//    public ItemStack getRecipeOutput()
//    {
//        return this.recipeOutput;
//    }
//
//    public NonNullList<ItemStack> getRemainingItems(InfusorCraftingInventory inv)
//    {
//        ItemStack[] aitemstack = new ItemStack[inv.getSizeInventory()];
//
//        for (int i = 0; i < aitemstack.length; ++i)
//        {
//            ItemStack itemstack = inv.getStackInSlot(i);
//            aitemstack[i] = net.minecraftforge.common.ForgeHooks.getContainerItem(itemstack);
//        }
//
//        return NonNullList.from(null, aitemstack);
//    }

//	@Override
//	public boolean matches(InfusorCraftingInventory inv, World worldIn) 
//	{
//		 List<ItemStack> list = Lists.newArrayList(this.recipeItems);
//
//		 for(int i =0; i < inv.getSizeInventory(); i++) {
//			 ItemStack itemstack = inv.getStackInSlot(i); //.getStackInRowAndColumn();
//			 
//			 if (itemstack != null)
//             {
//                 boolean flag = false;
//
//                 for (ItemStack itemstack1 : list)
//                 {
//                     if (itemstack.getItem() == itemstack1.getItem() 
//                     		&& (itemstack.getItem().getRegistryType() == itemstack1.getItem().getRegistryType()))
//                     {
//                         flag = true;
//                         list.remove(itemstack1);
//                         break;
//                     }
//                 }
//
//                 if (!flag)
//                 {
//                     return false;
//                 }
//             }
//		 }
//		 
////	        for (int i = 0; i < inv.getHeight(); ++i)
////	        {
////	            for (int j = 0; j < inv.getWidth(); ++j)
////	            {
////	                ItemStack itemstack = inv.getStackInSlot( (j * inv.getWidth()) + (i* inv.getHeight()) ); //.getStackInRowAndColumn();
////
////	                if (itemstack != null)
////	                {
////	                    boolean flag = false;
////
////	                    for (ItemStack itemstack1 : list)
////	                    {
////	                        if (itemstack.getItem() == itemstack1.getItem() 
////	                        		&& (itemstack.getItem().getRegistryType() == itemstack1.getItem().getRegistryType()))
////	                        {
////	                            flag = true;
////	                            list.remove(itemstack1);
////	                            break;
////	                        }
////	                    }
////
////	                    if (!flag)
////	                    {
////	                        return false;
////	                    }
////	                }
////	            }
////	        }
//
//	        return list.isEmpty();
//	}

//	@Override
//	public ItemStack getCraftingResult(InfusorCraftingInventory inv) {
//		// TODO Auto-generated method stub
//		return this.recipeOutput.copy();
//	}

//	@Override
//	public boolean canFit(int width, int height) {
//		// TODO Auto-generated method stub
//		return this.recipeItems.size() > (width + height);
//	}

//	@Override
//	public ResourceLocation getId() {
//		// TODO Auto-generated method stub
//		 return new ResourceLocation(Magelight.MODID);
//	}

//	@Override
//	public IRecipeSerializer getSerializer() {
//		// TODO Auto-generated method stub
//		return IRecipeSerializer.CRAFTING_SHAPELESS;
//	}
//

}