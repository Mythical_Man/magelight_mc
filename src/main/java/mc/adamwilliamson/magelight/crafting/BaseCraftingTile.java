package mc.adamwilliamson.magelight.crafting;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import mc.adamwilliamson.magelight.Constants;
import mc.adamwilliamson.magelight.container.BaseContainer;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public abstract class BaseCraftingTile extends TileEntity implements INamedContainerProvider 
{
	protected LazyOptional<IItemHandler> inventoryHandler = LazyOptional.of(this::createStackHadler);
	protected BaseCraftingManager<?> craftingManager;
	private BaseContainer _inv;
	
	public BaseCraftingTile(TileEntityType<?> tileEntityTypeIn, BaseCraftingManager<?> craftinManagerIn)
	{
		super(tileEntityTypeIn);
		this.craftingManager = craftinManagerIn;
	}

	protected abstract int getSlotCount();
	
	protected abstract int getResultSlot();
	
	protected abstract List<Integer> getIngredientSlots();
	
	public void setInventory(BaseContainer inv)
	{
		_inv = inv;
	}
	
	public BaseContainer getInventory()
	{
		return _inv;
	}
	
	protected BaseRecipe findMatchingRecipe()
	{
		return craftingManager.findMatchingRecipe(getInventory(), world);
	}
	
	protected Boolean canCraft()
	{
		BaseRecipe recipe = findMatchingRecipe();
		if (recipe == null) return false;
		
		return true;
	}
	
	public void craft()
	{
		BaseRecipe recipe = findMatchingRecipe();
		
		if (recipe == null)
		{
			return;
		}
		
		ItemStack result = recipe.getCraftingResult(null);
		
		Logger.getGlobal().info(result.getMaxStackSize() + "");
		
		if (this.getInventory().getStackInSlot(getResultSlot()).isEmpty()) 
		{
			Logger.getGlobal().info("stack is empty, setting");
			this.getInventory().setInventorySlotContents(getResultSlot(), recipe.getCraftingResult(null));
		}
		else if (Container.areItemsAndTagsEqual(this.getInventory().getStackInSlot(getResultSlot()), result)
				&& result.getMaxStackSize() > result.getCount() )
		{
			Logger.getGlobal().info("stack is FULL, incrementing");
			this.getInventory().getStackInSlot(getResultSlot()).grow(1);
		} 
		else 
		{
			//  Don't craft
			return;
		}
		
		this.getIngredientSlots().forEach(c -> this.getInventory().decrStackSize(c,  1));
	}
	
	protected IItemHandler createStackHadler()
	{
		ItemStackHandler handler = new ItemStackHandler(getSlotCount())
		{
			@Override
			protected void onContentsChanged(int slot) 
			{
				markDirty();
			}
		};
		
		return handler;
	}
	
	@Nonnull
	@Override
	public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side)
	{
		if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.orEmpty(cap, inventoryHandler);
		}
		
		return super.getCapability(cap, side);
	}

	@SuppressWarnings("unchecked")
	private INBTSerializable<CompoundNBT> toSerializableCompound(IItemHandler h)
	{
		return (INBTSerializable<CompoundNBT>)h;
	}
	
	@Override
	public void read(CompoundNBT tag)
	{
		CompoundNBT invTag = tag.getCompound(Constants.Inventory);
		inventoryHandler.ifPresent(h -> toSerializableCompound(h).deserializeNBT(invTag));
		super.read(tag);
	}
	
	@Override
	public CompoundNBT write(CompoundNBT tag)
	{
		inventoryHandler.ifPresent(h -> {
			CompoundNBT compound =  toSerializableCompound(h).serializeNBT();
			tag.put(Constants.Inventory, compound);
		});
		
		return super.write(tag);
	}
	
	@Override
	public ITextComponent getDisplayName()
	{
		return new TranslationTextComponent(getType().getRegistryName().toString());
	}
}