package mc.adamwilliamson.magelight.crafting;

import java.util.Optional;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public interface InfusorRecipeType extends IRecipeType<IInfusorRecipe> {
	IRecipeType<IInfusorRecipe> INFUSOR = register("infusor");
	
	static <T extends IRecipe<?>> IRecipeType<T> register(final String key) {
		return Registry.register(Registry.RECIPE_TYPE, new ResourceLocation(key), new IRecipeType<T>() {
			public String toString() {
				return key;
			}
		});
	}

	default <C extends IInventory> Optional<IInfusorRecipe> matches(IRecipe<C> recipe, World worldIn, C inv) {
		return recipe.matches(inv, worldIn) ? Optional.of((IInfusorRecipe)recipe) : Optional.empty();
	}
}
