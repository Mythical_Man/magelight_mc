package mc.adamwilliamson.magelight.crafting;

import java.util.List;
import java.util.logging.Logger;

import mc.adamwilliamson.magelight.Magelight;
import mc.adamwilliamson.magelight.container.BaseContainer;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public abstract class BaseRecipe implements IRecipe<BaseContainer>
{
    protected final ItemStack recipeOutput;
    protected final List<ItemStack> recipeItems;

    public BaseRecipe(ItemStack output, List<ItemStack> inputList)
    {
        this.recipeOutput = output;
        this.recipeItems = inputList;
    }

    public ItemStack getRecipeOutput()
    {
        return this.recipeOutput;
    }

//    public NonNullList<ItemStack> getRemainingItems(BaseContainer inv)
//    {
//        ItemStack[] aitemstack = new ItemStack[inv.getSizeInventory()];
//
//        for (int i = 0; i < aitemstack.length; ++i)
//        {
//            ItemStack itemstack = inv.getStackInSlot(i);
//            aitemstack[i] = net.minecraftforge.common.ForgeHooks.getContainerItem(itemstack);
//        }
//
//        return NonNullList.from(null, aitemstack);
//    }

    protected abstract List<Integer> getCraftingSlots();
    
	@Override
	public boolean matches(BaseContainer inv, World worldIn) 
	{
		//ArrayList<Object> list = Lists.newArrayList(this.recipeItems);//Lists.newArrayList((this.recipeItems).stream().map(i -> i.getItem() ).toArray());
 		if (inv == null) return false;
 		//if (list.isEmpty()) return false;
 		
 		if (getIngredients().size() < this.getCraftingSlots().stream().filter(o -> inv.getStackInSlot(o).isEmpty()).count())
 		{
 			Logger.getGlobal().info("Recipe Ingregient slots too low.");
 			return false;
 		}
 		
 		
 		for( int i : getCraftingSlots())
		{
			ItemStack itemstack = inv.getStackInSlot(i);
			 
			 if (itemstack == null || itemstack.isEmpty()) continue;
			 
//             if (inv.areItemsAndTagsEqual(stack1, stack2)!list.contains(itemstack.getItem())) 
//             {
//            	 return false;
//             }
             
			 if (!this.recipeItems.stream().anyMatch(o -> itemstack.getItem() == o.getItem()))
				 Logger.getGlobal().info("Items dont match 1");
			 
			 Logger.getGlobal().info(this.recipeItems.get(0).getItem().getTranslationKey());
			 Logger.getGlobal().info(this.recipeItems.get(1).getItem().getTranslationKey());
			 
			 Logger.getGlobal().info(this.recipeItems.get(0).getItem().getTranslationKey());
			 Logger.getGlobal().info(this.recipeItems.get(1).getItem().getTranslationKey());
			 
			 if (!this.recipeItems.stream().anyMatch(o -> ItemStack.areItemStackTagsEqual(itemstack, o)))
				 Logger.getGlobal().info("Item stack tags dont match 2");
			 
             //if (!this.recipeItems.stream().anyMatch(o -> Container.areItemsAndTagsEqual(itemstack, o)))
			 if (!this.recipeItems.stream().anyMatch(o -> Container.areItemsAndTagsEqual(itemstack, o)))
             {
            	 Logger.getGlobal().info("items dont match recipe"); 
            	 return false;
             }
			    
//             for (ItemStack itemstack1 : list)
//             {
//                 if ()
//                 {
//                	 Logger.getGlobal().info("matchies: item match");
//                     flag = true;
//                     list.remove(itemstack1);
//                     break;
//                 }
//             }
//
//             if (!flag)
//             {
//                 return false;
//             }
		 }
		 return true;
	}

	@Override
	public ItemStack getCraftingResult(BaseContainer inv)
	{
		return this.recipeOutput.copy();
	}

	@Override
	public boolean canFit(int width, int height)
	{
		return this.recipeItems.size() > (width * height);
	}

	@Override
	public ResourceLocation getId()
	{
		 return new ResourceLocation(Magelight.MODID);
	}
}