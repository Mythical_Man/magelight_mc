package mc.adamwilliamson.magelight.crafting;

import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeType;

public interface IInfusorRecipe extends IRecipe<InfusorCraftingInventory>
{
	default IRecipeType<?> getType() 
	{
		return InfusorRecipeType.INFUSOR;
	}
}