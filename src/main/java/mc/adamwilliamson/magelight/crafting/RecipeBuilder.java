package mc.adamwilliamson.magelight.crafting;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class RecipeBuilder 
{
	public class ShapelessRecipeBase
	{
		public List<ItemStack> list = Lists.<ItemStack>newArrayList();
		public ItemStack output = null;
	}
	ShapelessRecipeBase base = new ShapelessRecipeBase();
	
	
	public RecipeBuilder addInput(Block block) 
	{
		base.list.add(new ItemStack(block));
		return this;
	}
	
	public RecipeBuilder addInput(ItemStack stack) 
	{
		base.list.add(stack.copy());
		return this;
	}
	
	public RecipeBuilder addInput(Item item) 
	{
		base.list.add(new ItemStack(item));
		return this;
	}
	
	public RecipeBuilder setOutput(ItemStack stack) {
		base.output = stack.copy();
		return this;
	}
	
	public RecipeBuilder setOutput(Block block) {
		base.output = new ItemStack(block);
		return this;
	}
	
	public RecipeBuilder setOutput(Item item) {
		base.output = new ItemStack(item);
		return this;
	}
	
	public ShapelessRecipeBase build() {
		assert base.output != null;
		assert base.list.size() > 1;
		
		return base;
	}
}