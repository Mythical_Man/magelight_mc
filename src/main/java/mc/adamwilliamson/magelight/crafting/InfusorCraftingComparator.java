//package mc.adamwilliamson.magelight.crafting;
//
//import java.util.Comparator;
//
//import net.minecraft.item.crafting.IRecipe;
//
//public class InfusorCraftingComparator implements Comparator{
//
//	final InfusorCraftingManager altarCrafting;
//	
//	public InfusorCraftingComparator(InfusorCraftingManager acm) {
//		this.altarCrafting = acm;
//	}
//	
//	public int compareRecipes(IRecipe recipe1, IRecipe recipe2){
//		if (recipe1 instanceof InfusorShapelessRecipe && !(recipe2 instanceof InfusorShapelessRecipe)) {
//			return 1;
//		}
//		if (recipe2 instanceof InfusorShapelessRecipe && !(recipe1 instanceof InfusorShapelessRecipe)) {
//			return -1;
//		}
//		if (recipe1.getType() == recipe2.getType()) {
//			return 0;
//		}
//		return -1;
//		
////		return recipe1 instanceof InfusorShapelessRecipes 
////				&& recipe2 instanceof InfusorShapedRecipes 
////				? 1 
////				: (recipe2 instanceof InfusorShapelessRecipes && recipe1 instanceof InfusorShapedRecipes 
////						? -1 
////						: (recipe2.getRecipeSize() < recipe1.getRecipeSize() 
////								? -1 
////								: (recipe2.getRecipeSize() > recipe2.getRecipeSize() 
////										? 1 
////										: 0)));
//	}
//	
//	
//	@Override
//	public int compare(Object o1, Object o2) {
//		return compareRecipes((IRecipe) o1, (IRecipe) o2);
//	}
//}