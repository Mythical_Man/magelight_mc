package mc.adamwilliamson.magelight.crafting;

import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.google.common.collect.Lists;

import mc.adamwilliamson.magelight.container.BaseContainer;

public class BaseCraftingManager<T extends BaseRecipe>
{
    protected final List<T> recipes = Lists.<T>newArrayList();
    
    public void addRecipe(T recipe)
    {
        this.recipes.add(recipe);
    }

    public T findMatchingRecipe(BaseContainer inventory, World worldIn)
    {
        for (T irecipe : this.recipes)
        {
            if (irecipe.matches(inventory, worldIn))
            {
            	return irecipe;
            }
        }

        return null;
    }

    //getRemainingItems?
    public ItemStack[] func_180303_b(BaseContainer inventory, World worldIn)
    {
        for (T irecipe : this.recipes)
        {
            if (irecipe.matches(inventory, worldIn))
            {
                return ((ItemStack[]) irecipe.getRemainingItems(inventory).toArray());
            }
        }

        ItemStack[] aitemstack = new ItemStack[inventory.getSizeInventory()];

        for (int i = 0; i < aitemstack.length; ++i)
        {
            aitemstack[i] = inventory.getStackInSlot(i);
        }

        return aitemstack;
    }

    public List<T> getRecipeList()
    {
        return this.recipes;
    }
}