package mc.adamwilliamson.magelight.crafting;

import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.ShapelessRecipe;

public abstract class BaseShapelessRecipe  extends BaseRecipe
{
    protected BaseShapelessRecipe(RecipeBuilder.ShapelessRecipeBase base)
    {
    	super(base.output, base.list);
    }
    
	@Override
	public IRecipeSerializer<ShapelessRecipe> getSerializer() 
	{
		return IRecipeSerializer.CRAFTING_SHAPELESS;
	}
}