//package mc.adamwilliamson.magelight.crafting;
//
//import java.util.logging.Logger;
//
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.inventory.IInventory;
//import net.minecraft.inventory.IRecipeHelperPopulator;
//import net.minecraft.inventory.container.Container;
//import net.minecraft.item.ItemStack;
//import net.minecraft.item.crafting.RecipeItemHelper;
//import net.minecraft.util.NonNullList;
//
//public class BaseCraftingInventory implements IInventory, IRecipeHelperPopulator 
//{
//   protected final NonNullList<ItemStack> stackList;
//   protected final int width;
//   protected final int height;
//   protected final Container container;
//
//	public BaseCraftingInventory(Container containerIn, int width, int height) 
//	{
//		this.stackList = containerIn.getInventory();
//		this.container = containerIn;
//		this.width = width;
//		this.height = height;
//	}
//
//	public void markDirty() {}
//
//	public int getHeight() 
//	{
//		return this.height;
//	}
//
//	public int getWidth() 
//	{
//		return this.width;
//	}
//
//	@Override
//	public void clear()
//	{
//		stackList.clear();
//	}
//
//	@Override
//	public void fillStackedContents(RecipeItemHelper helper)
//	{
//		Logger.getGlobal().info("fillStackContents:  Being calles, is EMPTY");
//		Logger.getGlobal().severe("");
//	}
//	
//	@Override
//	public int getSizeInventory()
//	{
//		return stackList.size();
//	}
//	
//	@Override
//	public boolean isEmpty()
//	{
//		return stackList.isEmpty();
//	}
//	
//	@Override
//	public ItemStack getStackInSlot(int index)
//	{
//		return stackList.get(index);
//	}
//	
//	@Override
//	public ItemStack decrStackSize(int index, int count)
//	{
//		stackList.get(index).getStack().shrink(count);
//		return stackList.get(index); 
//	}
//	
//	@Override
//	public ItemStack removeStackFromSlot(int index)
//	{
//		return stackList.set(index, ItemStack.EMPTY);
//	}
//	
//	@Override
//	public void setInventorySlotContents(int index, ItemStack stack)
//	{
//		stackList.set(index, stack);
//	}
//	
//	@Override
//	public boolean isUsableByPlayer(PlayerEntity player)
//	{
//		return true;
//	}
//
////   public void fillStackedContents(RecipeItemHelper helper) 
////   {
////      for(ItemStack itemstack : this.stackList) 
////      {
////         helper.accountPlainStack(itemstack);
////      }
////   }
//}