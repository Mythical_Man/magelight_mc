package mc.adamwilliamson.magelight.crafting.infusor;

import mc.adamwilliamson.magelight.blocks.ModBlocks;
import mc.adamwilliamson.magelight.crafting.BaseCraftingManager;
import mc.adamwilliamson.magelight.crafting.BaseRecipe;
import mc.adamwilliamson.magelight.crafting.InfusorShapelessRecipe;
import mc.adamwilliamson.magelight.crafting.RecipeBuilder;

public class InfusorCraftingManager extends BaseCraftingManager<BaseRecipe> 
{
    private static final InfusorCraftingManager instance = new InfusorCraftingManager();

    public static InfusorCraftingManager getInstance()
    {
        return instance;
    }

    private InfusorCraftingManager()
    {
    	super();

    	initRecipes();
    }
    
    public void initRecipes()
    {
    	addShapelessRecipe(
			new RecipeBuilder()
				.addInput(ModBlocks.MYSTIC_FUNGUS_DISSOLVE)
				.addInput(ModBlocks.MYSTIC_FUNGUS_SOLID)
				.setOutput(ModBlocks.MYSTIC_FUNGUS_REPRESS)
				.build());
    }
    
    public void addShapelessRecipe(RecipeBuilder.ShapelessRecipeBase base) 
    {
    	this.addRecipe(new InfusorShapelessRecipe(base));
    }
}