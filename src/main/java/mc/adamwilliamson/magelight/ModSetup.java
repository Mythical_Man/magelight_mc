package mc.adamwilliamson.magelight;

import mc.adamwilliamson.magelight.blocks.ModBlocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class ModSetup {

	public ItemGroup itemGroup = new ItemGroup("magelight") {
		public ItemStack createIcon() {
			return new ItemStack(ModBlocks.FIRSTBLOCK);
		}
	};
	
	public void init() {
		
	}
}
