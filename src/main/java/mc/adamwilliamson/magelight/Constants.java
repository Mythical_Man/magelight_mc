package mc.adamwilliamson.magelight;


import java.util.ArrayList;

import java.util.Collections;

import java.util.List;


import net.minecraft.item.Item;

import net.minecraft.item.Items;


public class Constants {

	public static final String Energy = "energy";

	public static final String Power = "power";

	public static final String Inventory = "inv";

	public static final String Heat = "heat";

	

	

	public static List<Item> FUELS;

	static{

	    ArrayList<Item> tmp = new ArrayList<Item>();

	    tmp.add(Items.COAL);

	    tmp.add(Items.CHARCOAL);

	    tmp.add(Items.COAL_BLOCK);

	    tmp.add(Items.LAVA_BUCKET);

	    FUELS = Collections.unmodifiableList(tmp);

	}

	

	public static List<Item> XP_ITEMS;

	static{

	    ArrayList<Item> tmp = new ArrayList<Item>();

	    tmp.add(Items.EXPERIENCE_BOTTLE);

	    tmp.add(Items.REDSTONE_BLOCK);

	    XP_ITEMS = Collections.unmodifiableList(tmp);

	}

}
