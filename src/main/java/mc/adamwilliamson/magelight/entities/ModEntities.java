package mc.adamwilliamson.magelight.entities;

import net.minecraft.entity.EntityType;
import net.minecraftforge.registries.ObjectHolder;

public class ModEntities {
	@ObjectHolder("magelight:weirdmob")
	public static EntityType<WeirdMobEntity> WEIRDMOB;
}
