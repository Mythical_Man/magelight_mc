package mc.adamwilliamson.magelight.entities;

import javax.annotation.Nullable;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.world.World;

public class WeirdMobEntity extends AnimalEntity {

	public WeirdMobEntity(EntityType<? extends AnimalEntity> type, World worldIn) { super(type,worldIn); }
	
	@Nullable
	@Override
	public AgeableEntity createChild(AgeableEntity ageable) {return null; }
}
