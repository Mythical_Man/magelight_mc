package mc.adamwilliamson.magelight.entities;

import javax.annotation.Nullable;

import mc.adamwilliamson.magelight.Magelight;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class WeirdMobRenderer extends MobRenderer<WeirdMobEntity, WeirdMobModel>{
	private static final ResourceLocation TEXTURE = new ResourceLocation(Magelight.MODID, "textures/entity/weirdmob.png");
	
	public WeirdMobRenderer(EntityRendererManager manager) { super(manager, new WeirdMobModel(), 0.5f); }  // 0.5 shadow size
	
	@Nullable
	@Override
	protected ResourceLocation getEntityTexture(WeirdMobEntity entity) { return TEXTURE; }

}
