package mc.adamwilliamson.magelight.tools;

public interface IHeatStorage
{
    int receiveHeat(int maxReceive, boolean simulate);

    int extractHeat(int maxExtract, boolean simulate);

    int getHeatStored();

    int getMaxHeatStored();

    boolean canExtract();

    boolean canReceive();

}