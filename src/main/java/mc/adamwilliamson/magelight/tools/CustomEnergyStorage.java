package mc.adamwilliamson.magelight.tools;

import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.energy.EnergyStorage;

public class CustomEnergyStorage extends EnergyStorage implements INBTSerializable<CompoundNBT>{

	public CustomEnergyStorage(int capacity, int maxTransfer) {
		super(capacity, maxTransfer);
		// TODO Auto-generated constructor stub
	}
	
	public void setEnergy(int energy) {
		this.energy = energy;
	}

	public void addEnergy(int energy) {
		this.energy += energy;
		if (this.energy > getMaxEnergyStored()) {
			this.energy = getMaxEnergyStored();
		}
	}
	
	public void consumeEnergy(int energy) {
		this.energy -= energy;
		if (this.energy < 0) {
			this.energy = 0;
		}
	}

	@Override
	public CompoundNBT serializeNBT() {
		// TODO Auto-generated method stub
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("energy", getEnergyStored());
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		// TODO Auto-generated method stub
		this.setEnergy(nbt.getInt("energy"));
	}
}
