package mc.adamwilliamson.magelight.tools;

import net.minecraft.nbt.INBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.*;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;

public class CapabilityHeat
{
    @CapabilityInject(IEnergyStorage.class)
    public static Capability<IEnergyStorage> HEAT = null;

    public static void register()
    {
        CapabilityManager.INSTANCE.register(IEnergyStorage.class, new IStorage<IEnergyStorage>()
        {
            @Override
            public INBT writeNBT(Capability<IEnergyStorage> capability, IEnergyStorage instance, Direction side)
            {
                return new IntNBT(instance.getEnergyStored());
            }

            @Override
            public void readNBT(Capability<IEnergyStorage> capability, IEnergyStorage instance, Direction side, INBT nbt)
            {
                if (!(instance instanceof EnergyStorage))
                    throw new IllegalArgumentException("Can not deserialize to an instance that isn't the default implementation");
                EnergyStorage es = ((EnergyStorage)instance);
                es.extractEnergy(es.getEnergyStored(), false);
                es.receiveEnergy(((IntNBT)nbt).getInt(), false);
            }
        },
        () -> new EnergyStorage(1000));
    }
}