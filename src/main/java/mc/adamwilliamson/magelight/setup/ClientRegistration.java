package mc.adamwilliamson.magelight.setup;

import mc.adamwilliamson.magelight.items.ModItems;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import mc.adamwilliamson.magelight.Magelight;
import net.minecraftforge.api.distmarker.Dist;

@Mod.EventBusSubscriber(modid = Magelight.MODID,value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientRegistration {

	@SubscribeEvent
	public static void onItemColor(ColorHandlerEvent.Item event) {
		event.getItemColors().register((stack, i) -> 0xff0000, ModItems.WEIRDMOB_EGG);
	}
}
