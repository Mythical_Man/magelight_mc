package mc.adamwilliamson.magelight.setup;

import mc.adamwilliamson.magelight.blocks.Infusor_Screen;
import mc.adamwilliamson.magelight.blocks.ModBlocks;
import mc.adamwilliamson.magelight.blocks.Mystic_Generator_Screen;
import mc.adamwilliamson.magelight.entities.WeirdMobEntity;
import mc.adamwilliamson.magelight.entities.WeirdMobRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class ClientProxy implements IProxy {

	@Override
	public void init() {
		ScreenManager.registerFactory(ModBlocks.MYSTIC_GENERATOR_CONTAINER, Mystic_Generator_Screen::new);
		ScreenManager.registerFactory(ModBlocks.INFUSOR_CONTAINER, Infusor_Screen::new);
		RenderingRegistry.registerEntityRenderingHandler(WeirdMobEntity.class,  WeirdMobRenderer::new);
	}
	
	@Override
	public World getClientWorld() {
		// TODO Auto-generated method stub
		return Minecraft.getInstance().world;
	}

	public PlayerEntity getPlayer() {
		return Minecraft.getInstance().player;
	}
}
