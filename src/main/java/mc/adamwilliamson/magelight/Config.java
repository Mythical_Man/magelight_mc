package mc.adamwilliamson.magelight;


import java.nio.file.Path;


import com.electronwill.nightconfig.core.file.CommentedFileConfig;

import com.electronwill.nightconfig.core.io.WritingMode;


import net.minecraftforge.common.ForgeConfigSpec;

import net.minecraftforge.eventbus.api.SubscribeEvent;

import net.minecraftforge.fml.common.Mod;

import net.minecraftforge.fml.config.ModConfig;


@Mod.EventBusSubscriber

public class Config {

	public static final String CATEGORY_GENERAL = "general";

	public static final String CATEGORY_POWER = "power";

	public static final String CATEGORY_HEAT = "heat";

	public static final String SUBCATEGORY_MYSTIC_GENERATOR = "mystic_generator";

	public static final String SUBCATEGORY_WEAK_POWER = "weak_power_generation";

	public static final String SUBCATEGORY_WEAK_HEAT = "weak_heat";

	public static final String SUBCATEGORY_INFUSOR_POWER = "infusor_power";

	

	public static final ForgeConfigSpec.Builder COMMON_BUILDER = new ForgeConfigSpec.Builder();

	public static final ForgeConfigSpec.Builder CLIENT_BUILDER = new ForgeConfigSpec.Builder();

	

	public static ForgeConfigSpec COMMON_CONFIG;

	public static ForgeConfigSpec CLIENT_CONFIG;

	

	public static ForgeConfigSpec.IntValue WEAK_POWER_GENERATE_XPBOTTLE;

	public static ForgeConfigSpec.IntValue WEAK_TICKS_PER_XPBOTTLE;

	

	public static ForgeConfigSpec.IntValue MYSTIC_GENERATOR_POWER_MAXPOWER;

	public static ForgeConfigSpec.IntValue MYSTIC_GENERATOR_POWER_GENERATE;

	public static ForgeConfigSpec.IntValue MYSTIC_GENERATOR_POWER_SEND;

	public static ForgeConfigSpec.IntValue MYSTIC_GENERATOR_TICKS_PER_XPBOTTLE;

	

	public static ForgeConfigSpec.IntValue WEAK_POWER_MAXPOWER;

	public static ForgeConfigSpec.IntValue WEAK_HEAT_MAX;

	public static ForgeConfigSpec.IntValue WEAK_HEAT_GENERATE_COAL;

	public static ForgeConfigSpec.IntValue WEAK_TICKS_PER_COAL;

	

	

	public static final String MAXPOWER = "maxPower";

	public static final int WEAK_MAX_POWER_STORAGE = 1000;

	public static final int WEAK_MAX_HEAT_STORAGE = 1000;

	

	

	static {

		COMMON_BUILDER

			.comment("General Settings")

			.push(CATEGORY_GENERAL);

		

		COMMON_BUILDER.pop();

		

		COMMON_BUILDER

			.comment("Power settings")

			.push(CATEGORY_POWER);

		

		setupWeakPower();

		setupMystic_Generator();

		

		setupInfusor();

		

		COMMON_BUILDER.pop();

		

		

		COMMON_BUILDER

			.comment("Power settings")

			.push(CATEGORY_POWER);

		

		setupWeakHeat();

			

		COMMON_BUILDER.pop();

		

		

		COMMON_CONFIG = COMMON_BUILDER.build();

		CLIENT_CONFIG = CLIENT_BUILDER.build();

	}

	

	private static void setupWeakPower() 

	{

		COMMON_BUILDER

			.comment("Weak Power Settings")

			.push(SUBCATEGORY_WEAK_POWER);

		

		WEAK_TICKS_PER_XPBOTTLE = COMMON_BUILDER

				.comment("Ticks for Weak power generators to generate power from Experience Bottles")

				.defineInRange("weakTicksPerXPBottle", 30, 0, Integer.MAX_VALUE);

		

		WEAK_POWER_GENERATE_XPBOTTLE= COMMON_BUILDER

				.comment("Weak power generation from XP Bottles")

				.defineInRange("weakXPBottlePowerGeneration", 50, 0, Integer.MAX_VALUE);

		

		COMMON_BUILDER.pop();

	}

	

	private static void setupWeakHeat() 

	{

		COMMON_BUILDER

			.comment("Weak Heat Settings")

			.push(SUBCATEGORY_WEAK_HEAT);

		

		WEAK_TICKS_PER_COAL =  COMMON_BUILDER

				.comment("Ticks for Weak heat generators to generate heat from Coal")

				.defineInRange("weakTicksPerCoal", 30, 0, Integer.MAX_VALUE);

		

		WEAK_HEAT_MAX = COMMON_BUILDER

				.comment("Max Heat storage")

				.defineInRange("weakHeatMaxStorage", 1000, 0, Integer.MAX_VALUE);

		

		WEAK_HEAT_GENERATE_COAL = COMMON_BUILDER

				.comment("Weak heat generation from Coal")

				.defineInRange("weakHeatGenerationPerCoal", 100, 0, Integer.MAX_VALUE);

		

		COMMON_BUILDER.pop();

	}

	

	private static void setupMystic_Generator() {

		COMMON_BUILDER

			.comment("MYSTIC_GENERATOR Settings")

			.push(SUBCATEGORY_MYSTIC_GENERATOR);

		

		MYSTIC_GENERATOR_POWER_MAXPOWER = COMMON_BUILDER.comment("Maximum Power for Mystic Generator")

				.defineInRange(MAXPOWER, 10000, 0, Integer.MAX_VALUE);

		

		MYSTIC_GENERATOR_POWER_GENERATE = COMMON_BUILDER.comment("Power generation for Mystic Generator")

				.defineInRange("generate", 100, 0, Integer.MAX_VALUE); 

		

		MYSTIC_GENERATOR_POWER_SEND = COMMON_BUILDER.comment("Power send for Mystic Generator")

				.defineInRange("send", 10, 0, Integer.MAX_VALUE);

		

		MYSTIC_GENERATOR_TICKS_PER_XPBOTTLE = COMMON_BUILDER.comment("Ticks for Mystic Generator to generate power from ExperienceBottles")

				.defineInRange("ticksPerXPBottle", 20, 0, Integer.MAX_VALUE);

		

		COMMON_BUILDER.pop();

	}

	

	private static void setupInfusor() {

		COMMON_BUILDER

			.comment("INFUSOR Power Settings")

			.push(SUBCATEGORY_INFUSOR_POWER);

		

		WEAK_POWER_MAXPOWER = COMMON_BUILDER

				.comment("Mxaimum Power for Infusor")

				.defineInRange(MAXPOWER, WEAK_MAX_POWER_STORAGE, 0, Integer.MAX_VALUE);

		

		COMMON_BUILDER.pop();

	}

	

	public static void loadConfig(ForgeConfigSpec spec, Path path) {

		final CommentedFileConfig configData = CommentedFileConfig.builder(path)

				.sync()

				.autosave()

				.writingMode(WritingMode.REPLACE)

				.build();

		

		configData.load();

		spec.setConfig(configData);

	}

	

	@SubscribeEvent

	public static void onLoad(final ModConfig.ConfigReloading configEvent) {

		

	}

	

	@SubscribeEvent

	public static void onReload(final ModConfig.ConfigReloading configEvent) {

		

	}

	

}
