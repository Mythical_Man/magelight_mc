package mc.adamwilliamson.magelight.datagen;

import java.util.function.Consumer;

import mc.adamwilliamson.magelight.blocks.ModBlocks;
import mc.adamwilliamson.magelight.items.ModItems;
import net.minecraft.advancements.criterion.InventoryChangeTrigger;
import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.data.RecipeProvider;
import net.minecraft.data.ShapedRecipeBuilder;

public class Recipes extends RecipeProvider {

	public Recipes(DataGenerator generatorIn) {super(generatorIn);}
	
	@Override
	protected void registerRecipes(Consumer<IFinishedRecipe> consumer) {
		ShapedRecipeBuilder.shapedRecipe(ModBlocks.MYSTIC_GENERATOR)
			.patternLine("wrw")
			.patternLine("igi")
			.patternLine("wrw")
			.key('i', Blocks.IRON_BLOCK)
			.key('r', Blocks.REDSTONE_BLOCK)
			.key('w', ModBlocks.SOCOTRA_PLANKS)
			.key('g', ModItems.MYSTIC_GEM)
			.setGroup("magelight")
			.addCriterion("mystic_gem", InventoryChangeTrigger.Instance.forItems(ModItems.MYSTIC_GEM))
			.build(consumer);
	}
}
