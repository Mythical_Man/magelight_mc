package mc.adamwilliamson.magelight.datagen;

import mc.adamwilliamson.magelight.blocks.ModBlocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.LootTableProvider;

public class LootTables extends BaseLootTableProvider {

	public LootTables(DataGenerator dataGeneratorIn) { super(dataGeneratorIn); }
	
	protected void addTables() {
		lootTables.put(ModBlocks.MYSTIC_GENERATOR, createStandardTable("mystic_generator", ModBlocks.MYSTIC_GENERATOR));
	}

}
