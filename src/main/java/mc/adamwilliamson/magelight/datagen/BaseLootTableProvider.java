package mc.adamwilliamson.magelight.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.data.LootTableProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.ConstantRange;
import net.minecraft.world.storage.loot.DynamicLootEntry;
import net.minecraft.world.storage.loot.ItemLootEntry;
import net.minecraft.world.storage.loot.LootParameterSets;
import net.minecraft.world.storage.loot.LootPool;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.LootTableManager;
import net.minecraft.world.storage.loot.functions.CopyName;
import net.minecraft.world.storage.loot.functions.CopyNbt;
import net.minecraft.world.storage.loot.functions.SetContents;

public abstract class BaseLootTableProvider extends LootTableProvider {

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
	
	protected final Map<Block, LootTable.Builder>lootTables = new HashMap<>();
	private final DataGenerator generator;
	
	public BaseLootTableProvider(DataGenerator dataGeneratorIn) {
		super(dataGeneratorIn);
		// TODO Auto-generated constructor stub
		this.generator = dataGeneratorIn;
	}
	
	protected abstract void addTables();
	
	protected LootTable.Builder createStandardTable(String name, Block block){
		LootPool.Builder builder =  LootPool.builder()
				.name(name)
				.rolls(ConstantRange.of(1))
				.addEntry(ItemLootEntry.builder(block)
						.acceptFunction(CopyName.func_215893_a(CopyName.Source.BLOCK_ENTITY))  //CopyName.builder(CopyName.Source.BLOCK_ENTITY) <-- on upgrade Mappings
						.acceptFunction(CopyNbt.func_215881_a(CopyNbt.Source.BLOCK_ENTITY)
								.func_216055_a("inv",  "BlockEntityTag.inv",  CopyNbt.Action.REPLACE)
								.func_216055_a("energy",  "BlockEntityTag.energy",  CopyNbt.Action.REPLACE))
						.acceptFunction(SetContents.func_215920_b()
								.func_216075_a(DynamicLootEntry.func_216162_a(new ResourceLocation("minecraft", "contents"))))
						);
						
		return LootTable.builder().addLootPool(builder);
	}
	
	@Override
	public void act(DirectoryCache cache) {
		addTables();
		
		Map<ResourceLocation, LootTable> tables = new HashMap<>();
		for(Map.Entry<Block, LootTable.Builder> entry: lootTables.entrySet()) {
			tables.put(entry.getKey().getLootTable(),entry.getValue().setParameterSet(LootParameterSets.BLOCK).build());
		}
		writeTables(cache,tables);
	}
	
	private void writeTables(DirectoryCache cache, Map<ResourceLocation, LootTable> tables) {
		Path outputFolder = this.generator.getOutputFolder();
		tables.forEach((key, lootTable) -> {
			Path path = outputFolder.resolve("data/" + key.getNamespace() + "/loot_tables/" + key.getPath() + ".json");
			try {
				IDataProvider.save(GSON, cache, LootTableManager.toJson(lootTable), path);
				
			}catch( IOException e) {
				//LOGGER.error("Couldn't write loto table {}", path, e);
			}
		});
	}
	
	@Override
	public String getName() { return "Magelight LootTables"; }

}
