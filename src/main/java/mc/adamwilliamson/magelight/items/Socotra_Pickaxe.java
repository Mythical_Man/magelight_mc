package mc.adamwilliamson.magelight.items;

import mc.adamwilliamson.magelight.Magelight;
import net.minecraft.item.Item;

public class Socotra_Pickaxe extends Item {
	public Socotra_Pickaxe() {
		super(new Item.Properties()
				.maxStackSize(1)
				.group(Magelight.setup.itemGroup)
		);
		
		setRegistryName("socotra_pickaxe");
	}

}
