package mc.adamwilliamson.magelight.items;

import java.util.Objects;

import mc.adamwilliamson.magelight.Magelight;
import mc.adamwilliamson.magelight.entities.ModEntities;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.SpawnReason;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tileentity.MobSpawnerTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.spawner.AbstractSpawner;

public class WeirdMobEggItem extends Item {
	public WeirdMobEggItem() {
		super(new Item.Properties().maxStackSize(1).group(Magelight.setup.itemGroup));
		setRegistryName("weirdmob_egg");
	}
	
	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		World world = context.getWorld();
		if (world.isRemote) {
			return ActionResultType.SUCCESS;
		} else {
			ItemStack itemstack = context.getItem();
			BlockPos blockpos = context.getPos();
			Direction direction = context.getFace();
			BlockState blockstate = world.getBlockState(blockpos);
			Block block = blockstate.getBlock();
			if (block == Blocks.SPAWNER) {
				TileEntity tileentity = world.getTileEntity(blockpos);
				if (tileentity instanceof MobSpawnerTileEntity) {
					AbstractSpawner abstractspawner = ((MobSpawnerTileEntity)tileentity).getSpawnerBaseLogic();
					abstractspawner.setEntityType(ModEntities.WEIRDMOB);
					tileentity.markDirty();
					world.notifyBlockUpdate(blockpos,  blockstate,  blockstate,  3);
					itemstack.shrink(1);;
					return ActionResultType.SUCCESS;
				}
			}
			
			BlockPos blockpos1;
			if (blockstate.getCollisionShape(world,  blockpos).isEmpty()) {
				blockpos1 = blockpos;
			}else {
				blockpos1 = blockpos.offset(direction);
			}
			
			if (ModEntities.WEIRDMOB.spawn(world,  itemstack, context.getPlayer(), blockpos1, SpawnReason.SPAWN_EGG, true, !Objects.deepEquals(blockpos,  blockpos1)) != null) {
				itemstack.shrink(1);
			}
			
			return ActionResultType.SUCCESS;
		}
		
	}

}
