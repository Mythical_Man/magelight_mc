package mc.adamwilliamson.magelight.items;

import mc.adamwilliamson.magelight.Magelight;
import net.minecraft.item.Item;

public class Socotra_Stick extends Item {
	public Socotra_Stick() {
		super(new Item.Properties()
				.maxStackSize(64)
				.group(Magelight.setup.itemGroup)
		);
		
		setRegistryName("socotra_stick");
	}

}
