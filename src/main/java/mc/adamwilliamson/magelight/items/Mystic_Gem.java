package mc.adamwilliamson.magelight.items;

import mc.adamwilliamson.magelight.Magelight;
import net.minecraft.item.Item;

public class Mystic_Gem extends Item {
	public Mystic_Gem() {
		super(new Item.Properties()
				.maxStackSize(16)
				.group(Magelight.setup.itemGroup)
		);
		
		setRegistryName("mystic_gem");
	}

}
