package mc.adamwilliamson.magelight.items;

import net.minecraftforge.registries.ObjectHolder;

public class ModItems {

	@ObjectHolder("magelight:mystic_gem")
	public static Mystic_Gem MYSTIC_GEM;
	
	@ObjectHolder("magelight:socotra_stick")
	public static Socotra_Stick SOCOTRA_STICK;
	
	@ObjectHolder("magelight:socotra_pickaxe")
	public static Socotra_Pickaxe SOCOTRA_PICKAXE;
	
	@ObjectHolder("magelight:weirdmob_egg")
	public static WeirdMobEggItem WEIRDMOB_EGG;
	
//	@ObjectHolder("magelight:mystic_solid_spores")
//	public static Mystic_Solid_Fungus_Spores MYSTIC_SOLID_SPORES;
}
